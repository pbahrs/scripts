# This file contains the terraform code that creates many resources
# in the cloud. The resources can then be tested using your preferred
# method.

variable "user" {
	default = "user1"
	description = "this is a user for the system"
}

variable access_key {
    type = string
}

variable secret_key {
    type = string
}

variable region {
    type = string
}

variable cloud_account_id {
    type = string
}
