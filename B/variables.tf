# This file contains the variables for the terraform input
# which are used at run time.

variable access_key{
    type = string
}

variable secret_key {
    type = string
}

variable region {
    type = string
}

variable cloud_account_id {
    type = string
}